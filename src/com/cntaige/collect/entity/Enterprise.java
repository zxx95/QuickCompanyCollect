package com.cntaige.collect.entity;

import java.io.Serializable;

public class Enterprise implements Serializable {
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 行业
     */
    private String category;
    /**
     * 公司规模
     */
    private String companyScale;
    /**
     * 公司类型
     */
    private String companyType;
    /**
     * 公司简介
     */
    private String companyIntr;
    /**
     * 社会统一代码
     */
    private String socialUniformCode;
    /**
     * 成立日期
     */
    private String establishDate;
    /**
     * 组织机构代码
     */
    private String organizationCode;
    /**
     * 经营期限
     */
    private String operatePeriod;
    /**
     * 登记机关
     */
    private String registerAuthority;
    /**
     * 经营状态
     */
    private String operateStatus;
    /**
     * 注册地址
     */
    private String registerAddress;
    /**
     * 注册资本
     */
    private String registerCapital;
    /**
     * 企业类型
     */
    private String enterpriseType;
    /**
     * 经营范围
     */
    private String businessScope;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCompanyScale() {
        return companyScale;
    }

    public void setCompanyScale(String companyScale) {
        this.companyScale = companyScale;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyIntr() {
        return companyIntr;
    }

    public void setCompanyIntr(String companyIntr) {
        this.companyIntr = companyIntr;
    }

    public String getSocialUniformCode() {
        return socialUniformCode;
    }

    public void setSocialUniformCode(String socialUniformCode) {
        this.socialUniformCode = socialUniformCode;
    }

    public String getEstablishDate() {
        return establishDate;
    }

    public void setEstablishDate(String establishDate) {
        this.establishDate = establishDate;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getOperatePeriod() {
        return operatePeriod;
    }

    public void setOperatePeriod(String operatePeriod) {
        this.operatePeriod = operatePeriod;
    }

    public String getRegisterAuthority() {
        return registerAuthority;
    }

    public void setRegisterAuthority(String registerAuthority) {
        this.registerAuthority = registerAuthority;
    }

    public String getOperateStatus() {
        return operateStatus;
    }

    public void setOperateStatus(String operateStatus) {
        this.operateStatus = operateStatus;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    public String getRegisterCapital() {
        return registerCapital;
    }

    public void setRegisterCapital(String registerCapital) {
        this.registerCapital = registerCapital;
    }

    public String getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(String enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    public String getBusinessScope() {
        return businessScope;
    }

    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope;
    }

    @Override
    public String toString() {
        return "Enterprise{" +
                "companyName='" + companyName + '\'' +
                ", category='" + category + '\'' +
                ", companyScale='" + companyScale + '\'' +
                ", companyType='" + companyType + '\'' +
                ", companyIntr='" + companyIntr + '\'' +
                ", socialUniformCode='" + socialUniformCode + '\'' +
                ", establishDate='" + establishDate + '\'' +
                ", organizationCode='" + organizationCode + '\'' +
                ", operatePeriod='" + operatePeriod + '\'' +
                ", registerAuthority='" + registerAuthority + '\'' +
                ", operateStatus='" + operateStatus + '\'' +
                ", registerAddress='" + registerAddress + '\'' +
                ", registerCapital='" + registerCapital + '\'' +
                ", enterpriseType='" + enterpriseType + '\'' +
                ", businessScope='" + businessScope + '\'' +
                '}';
    }
}